var webserver = require('gulp-webserver');
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserify = require('gulp-browserify');
var babel = require("gulp-babel");
var mocha = require('gulp-mocha');
var mochaBabel = require("mocha-babel");
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
    return gulp.src('./app/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('html', function () {
    return gulp.src('./app/index.html')
        .pipe(gulp.dest('./dist/'));
});


gulp.task('test', function () {
    return gulp.src('test/**/*Spec.js', {read: false})
        .pipe(mocha({reporter: 'nyan', compilers: {js: mochaBabel}}));
});

gulp.task('scripts', function() {
    return gulp.src('./app/app.js')
        .pipe(browserify({
            transform: ['babelify'],
            blacklist: ['useStrict'],
            insertGlobals : true,
            debug: true
        }))
        .pipe(gulp.dest('./dist/'))
});

gulp.task('serve', ['build'], function() {
    gulp.src('./dist')
        .pipe(webserver({
            host: '0.0.0.0',
            port: 4000,
            livereload: false,
            open: true,
            fallback: 'index.html'
        }));
});

gulp.task('watch', function () {
    gulp.watch('./app/**/*.scss', ['sass']);
    gulp.watch('./app/**/*.js', ['scripts']);
    gulp.watch('./app/**/*.html', ['html']);
});

gulp.task('default', ['serve', 'watch']);
gulp.task('build', ['sass', 'scripts', 'html']);