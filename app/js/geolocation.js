import $ from 'jquery';

export default class Geolocation{
    constructor() {
        if(navigator.geolocation) {
//            navigator.geolocation.getCurrentPosition(this.updatePosition.bind(this), () => { alert('x'); }, {enableHighAccuracy: true});
            navigator.geolocation.watchPosition(this.updatePosition.bind(this), () => { alert('x'); }, {enableHighAccuracy: true})
        }
        this._position = {};

        function compassHeading(alpha, beta, gamma) {

            // Convert degrees to radians
            var alphaRad = alpha * (Math.PI / 180);
            var betaRad = beta * (Math.PI / 180);
            var gammaRad = gamma * (Math.PI / 180);

            // Calculate equation components
            var cA = Math.cos(alphaRad);
            var sA = Math.sin(alphaRad);
            var cB = Math.cos(betaRad);
            var sB = Math.sin(betaRad);
            var cG = Math.cos(gammaRad);
            var sG = Math.sin(gammaRad);

            // Calculate A, B, C rotation components
            var rA = - cA * sG - sA * sB * cG;
            var rB = - sA * sG + cA * sB * cG;
            var rC = - cB * cG;

            // Calculate compass heading
            var compassHeading = Math.atan(rA / rB);

            // Convert from half unit circle to whole unit circle
            if(rB < 0) {
                compassHeading += Math.PI;
            }else if(rA < 0) {
                compassHeading += 2 * Math.PI;
            }

            // Convert radians to degrees
            compassHeading *= 180 / Math.PI;

            return compassHeading;

        }

        var that = this;

        window.addEventListener('deviceorientation', (evt) => {
            if(evt.absolute === true && evt.alpha !== null) {
                that.headingContainer2.innerHTML = compassHeading(evt.alpha, evt.beta, evt.gamma);
                if(evt.webkitCompassHeading) {
                    that.headingDegrees.innerHTML = evt.webkitCompassHeading;
                } else {
                    that.headingDegrees.innerHTML = event.alpha;
                }
            }

        }, false);
    }

    get lat() {
        return this._position.latitude || 0;
    }

    get lon() {
        return this._position.longitude || 0;
    }

    updatePosition(position) {
        this._position = position.coords;
        $(this).trigger('positionUpdated', {position: this._position});
    }
}