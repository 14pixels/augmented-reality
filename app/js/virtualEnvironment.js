import $ from 'jquery';
import THREE from 'THREE';
import '../vendor/DeviceOrientationControls';
import POICollection from './poiCollection';


export default class VirtualEnvironment{
    constructor(geolocation) {

        this.geolocation = geolocation;
        this.pois = [];

        this.poiCollection = new POICollection();
        $(this.poiCollection).on('newPois', (event, data) => {
            this.setPois(data.pois);
        });
        $(this.geolocation).on('positionUpdated', (event, data) => {
            this.poiCollection.updateDistances(data.position);
            this.updatePois();
        });

        this.container = $('#virtual-environment');
        this.initRenderer();
        this.setBaseScene();
        this.initControls();
        this.render();
    }

    initRenderer() {
        this.camera = new THREE.PerspectiveCamera(75, this.container.width() / this.container.height(), 1,1000000);
        this.camera.position.y = 1;
        this.renderer = new THREE.WebGLRenderer({ alpha: true });
        this.renderer.setSize(this.container.width(), this.container.height());
        this.container.append(this.renderer.domElement);

        window.addEventListener('resize', () => {
            this.camera.aspect = this.container.width() / this.container.height();
            this.camera.updateProjectionMatrix();
            this.renderer && this.renderer.setSize(this.container.width(), this.container.height());
        }, true);
    };

    addGridHelper() {
        let gridXZ = new THREE.GridHelper(100, 1);
        gridXZ.position.y = -3;
        this.scene.add(gridXZ);
    }

    setBaseScene() {
        this.scene = new THREE.Scene();
        let light = new THREE.AmbientLight( 0xFFFFFF );
        this.scene.add( light );
        this.addGridHelper();
        //this.addRandomCubes();
    };

    updateScene() {
        this.controls.update();
        this.renderer.render(this.scene, this.camera);
    };

    render() {
        //update animations
        this.updateScene();
        window.requestAnimationFrame(this.render.bind(this), this.container);

    };

    initControls() {
        this.controls = new THREE.DeviceOrientationControls(this.camera);
        this.controls.connect();
    };

    setLocalPosition(poi) {
        let factor = 30000;
        poi.position.x = (poi.gps.lon - this.geolocation.lon) * factor;
        poi.position.z = (poi.gps.lat - this.geolocation.lat) * factor;
    }

    getStandardCube(color) {
        let geometry = new THREE.BoxGeometry( 1, 5, 1 );
        let material = new THREE.MeshLambertMaterial( { color: color, shading: THREE.FlatShading, overdraw: 0.5 } );
        return new THREE.Mesh( geometry, material );
    }

    generatePOI(poi) {
        let threePOI = this.getStandardCube(poi.object.color);
        threePOI.gps = {
            lat: poi.lat,
            lon: poi.lon
        };
        this.setLocalPosition(threePOI);
        return threePOI;
    }

    removeOldPois() {
        for (let poi of this.pois) {
            this.scene.remove(poi);
        }
        this.pois = [];
    }

    setPois(pois = this.pois) {
        this.removeOldPois();
        for (let poi of pois) {
            let newPoi = this.generatePOI(poi);
            this.scene.add(newPoi);
            this.pois.push(newPoi);
        }
    }

    updatePois() {
        console.log('updatePois');
        for (let poi of this.pois) {
            this.setLocalPosition(poi);
        }
    }

}