export default class Environment{
    constructor() {

        this.n = navigator;
        this.setUserMedia();
        this.initMediaStream();

    }

    /**
     * set crossbrowser getUserMedia function
     */
    setUserMedia() {
        this.n.getUserMedia = (navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia);
    }

    initMediaStream() {
        if (typeof MediaStreamTrack === 'undefined'){
            alert('This browser does not support MediaStreamTrack.\n\nTry Chrome Canary.');
        } else {
            MediaStreamTrack.getSources(this.gotMediaSources.bind(this));
        }
    }

    getUserMediaStream(source) {
        if (this.n.getUserMedia) {
            this.n.getUserMedia({video: {
                optional: [{'sourceId': source.id}]
            }, audio: false}, this.onSuccess.bind(this), this.onError.bind(this));
        }
    }

    gotMediaSources(sources) {
        for (let i = 0; i !== sources.length; ++i) {
            //get userMedia if type is video and camera is facing the environment or is a webcam
            if(sources[i].kind === 'video' && sources[i].facing !== 'user') {
                this.getUserMediaStream(sources[i]);
            }
        }
    }

    onSuccess(stream) {
        let output = document.getElementById('environment-output');

        if (this.n.webkitGetUserMedia)
            return output.src = window.URL.createObjectURL(stream);

        return output.src = stream;

    }

    onError() {
        alert('This browser does not support getUserMedia.\n\nTry Chrome Canary.');
    }

}