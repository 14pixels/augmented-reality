import $ from 'jquery';

/**
 * lat = z
 * lon = x
 */
export default class POICollection{
    constructor() {
        this._pois = [
            {
                lat: 47.723129950680324,
                lon: 13.087066411972046,
                alt: 100,
                name: 'fh_wiese',
                desc: 'fh kreuzung auf der wiese zwischen fh und wohnheim',
                object: {
                    type: 'cube',
                    color: 'red'
                }
            },
            {
                lat: 47.72194988638612,
                lon: 13.08821439743042,
                alt: 100,
                name: 'wendeplatz',
                desc: 'wendeplatz wohnheim',
                object: {
                    type: 'cube',
                    color: 'blue'
                }
            },
            {
                lat: 47.72465280760033,
                lon: 13.085666298866272,
                alt: 100,
                name: 'meierei',
                desc: 'meierei',
                object: {
                    type: 'cube',
                    color: 'yellow'
                }
            },
            {
                lat: 47.722751033866494,
                lon: 13.089673519134521,
                alt: 100,
                name: 'kreisverkehr',
                desc: 'kreisverkehr spar',
                object: {
                    type: 'cube',
                    color: 'green'
                }
            },
            {
                lat: 47.72565238379361,
                lon: 13.08298408985138,
                alt: 100,
                name: 'kl_brücke',
                desc: 'vor kleiner Salzachbrücke',
                object: {
                    type: 'cube',
                    color: 'purple'
                }
            },
            {
                lat: 47.731548410455275,
                lon: 13.082742691040039,
                alt: 100,
                name: 'ursteinbrücke',
                desc: 'ursteinbrücke',
                object: {
                    type: 'cube',
                    color: 'pink'
                }
            }


        ];

        setTimeout(() => {
            $(this).trigger('newPois', {pois: this._pois});
        }, 1000);

    }

    calcDistanceInMeters(lat1, lon1, lat2, lon2) {  // generally used geo measurement function
        let R = 6378.137; // Radius of earth in KM
        let dLat = (lat2 - lat1) * Math.PI / 180;
        let dLon = (lon2 - lon1) * Math.PI / 180;
        let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
                Math.sin(dLon/2) * Math.sin(dLon/2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        let d = R * c;
        return parseInt(d * 1000); // meters
    }

    renderPois() {
        this.poisContainer = $('#pois');
        this.poisContainer.empty();
        for (let poi of this.pois) {
            this.poisContainer.append(`<div class="poi"><div class="color-indicator" style="background-color: ${poi.object.color}"></div><div class="poi-name">${poi.name} (${poi.distanceToOrigin}m)<div></div>`);
        }
    }

    updateDistances(position) {
        for (let poi of this.pois) {
            poi.distanceToOrigin = this.calcDistanceInMeters(poi.lat, poi.lon, position.latitude, position.longitude)
        }
        this.renderPois();
    }

    get pois() {
        return this._pois;
    }

    set pois(pois) {

    }

}