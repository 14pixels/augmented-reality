import $ from 'jquery';
import Environment from './js/environment';
import Geolocation from './js/geolocation';
import VirtualEnvironment from './js/virtualEnvironment';

class Application {
    constructor() {
        this.environment = new Environment();
        this.geolocation = new Geolocation();
        this.virtualEnvironment = new VirtualEnvironment(this.geolocation);

    }
}

$(() => {
    new Application();
});